import { Events, engine } from './../matter.js';

export function initCollision(){
        
    // an example of using collisionStart event on an engine
    Events.on(engine, 'collisionStart', function(event) {
        var pairs = event.pairs;

        // change object colours to show those starting a collision
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i];
            console.log(pair.bodyA.label);
            console.log(pair.bodyB.label);
            if(pair.bodyA.label == "ground" || pair.bodyB.label == "ground" ) console.log('GAMEOVER');
        }
    });

}