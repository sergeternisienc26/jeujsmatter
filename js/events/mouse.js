import { engine, render, World, Mouse, MouseConstraint } from './../matter.js';


export function initMouse(){
    
    let mouse = Mouse.create(render.canvas);
    let mouseConstraint = MouseConstraint.create(engine, {
        mouse: mouse,
        constraint: {
            render: {visible: true}
        }
    });

    render.mouse = mouse;
    World.add(engine.world,[mouseConstraint]);

    
    document.addEventListener('click', function(evt){
        console.log(evt);
    });

}
