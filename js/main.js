import {initMouse} from './events/mouse.js';
import {initWorld} from './world.js';
import {initCollision} from './events/collision.js';


function initGame(){
    initWorld();
    initMouse();
    initCollision();
}

initGame();
