import { engine, Bodies, World, render, Render } from './matter.js'

export function initWorld(){
    let ground = Bodies.rectangle(400,600,810,60,{ isStatic: true}); 
    ground.label = "ground";
    let boxA = Bodies.rectangle(400,200,80,80, {
        render: {
          sprite: {
          //  texture: 'images/box.png'
          }
        },
        isStatic:false
      });
    boxA.label = "boxA";
    let boxB = Bodies.rectangle(450,50,80,80, {
        render: {
          sprite: {
           // texture: 'images/box.png',
            xScale: 1,
            yScale: 1
          }
        },
        isStatic:false
      });
    boxB.label = "boxB";

    let boxC = Bodies.rectangle(500,50,80,80, {
      render: {
        sprite: {
         // texture: 'images/box.png',
          xScale: 1,
          yScale: 1
        }
      },
      isStatic:true
    });
   boxC.label = "boxC";

    let circleA = Bodies.circle(250,80,120, {isStatic : false});
    circleA.label = "circleA";   

    World.add(engine.world,[boxA,boxB,boxC,ground,circleA]);
    engine.set = 0;
    // Suivi de caméra sur un objet du monde
    /*setInterval(function(){
        Render.lookAt(render,boxA,{ x: 600, y: 600 });
    }, 10);*/
}